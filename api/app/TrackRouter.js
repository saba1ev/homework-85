const express = require('express');
const Track = require('../models/TrackModels');

const createRouter = () => {
  const router = express.Router();
  router.get('/', (req, res)=>{
    if (req.query.album){
      Track.find({album: req.query.album}).sort({number: 1})
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
    } else {
      Track.find().populate('album').sort({number: 1})
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
    }
  });
  router.post('/', (req, res) =>{
    const track = new Track(req.body);

    track.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error))
  });
  return router
};

module.exports = createRouter;