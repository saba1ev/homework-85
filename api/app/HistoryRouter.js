const express = require('express');
const Track_History = require('../models/HistoryModels');
const auth = require('../middleware/auth');

const createRouter = () =>{
  const router = express.Router();

  router.post('/', auth, async (req, res)=>{
    const user = User.findOne({token});
    if (!user){
      return res.status(401).send({error: "Login user"})
    }
    const newDate = new Date().toISOString();
    const track_history = new Track_History({ track:req.body.track, user: user._id, datetime: newDate});
    await track_history.save();
    res.send(track_history);
  });
  router.get('/', auth, (req, res) =>{
    Track_History.find({user: req.user._id}).sort('datetime')
      .populate(
        {path: 'track',
        populate:{path: 'album',
          populate: {path: 'artist'}
        }})
      .then(track => res.send(track))
      .catch(() => res.sendStatus(500))

  })
  return router;
};



module.exports = createRouter;