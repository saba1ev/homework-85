const express = require('express');
const User = require('../models/UserModels');

const createRouter = () =>{
  const router = express.Router();


  router.post('/', async (req, res) => {
    const user = new User(req.body);
    user.createToken();

    try {
      await user.save();
      return res.send({token: user.token});
    } catch (e) {
      return res.status(400).send(e)
    }
  });

  router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: `Username does not exist ${req.body.username}` });
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: `Password is wrong ${req.body.password}`});
    }

    user.createToken();
    await user.save();

    res.send({username: user.username, token: user.token});
  });
  return router;
};

module.exports = createRouter;