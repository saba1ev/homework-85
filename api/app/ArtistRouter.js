const express = require('express');
const Artist = require('../models/ArtistsModels');
const multer = require('multer');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();
  router.get('/', (req, res)=>{
    Artist.find()
      .then(result => res.send(result))
      .catch(()=> res.sendStatus(500));
  });
  router.post('/', upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
      albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error))
  });
  return router
};

module.exports = createRouter;