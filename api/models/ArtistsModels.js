const mongoose = require('mongoose');
const ArtistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  photo: String,
  information: String
});

const ArtistModule = mongoose.model('Artist', ArtistSchema);
module.exports = ArtistModule;