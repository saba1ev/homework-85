const mongoose = require('mongoose');

const TrackHistorySchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  track: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Track',
    required: true
  },
  datetime: {
    type: String,
    required: true
  }
});

const Track_historyModule = mongoose.model('Track_history', TrackHistorySchema);

module.exports = Track_historyModule;