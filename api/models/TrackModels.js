const mongoose = require('mongoose');

const TrackSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  album: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  duration: {
    type: String,
    required: true
  },
  number: {
    type: Number,
    required: true
  }
});
const TrackModule = mongoose.model('Track', TrackSchema);

module.exports = TrackModule;