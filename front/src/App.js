import React, {Component, Fragment} from 'react';
import Artists from "./Containers/Artists/Artists";
import {Switch, Route, NavLink as RouterLink} from 'react-router-dom';
import Albums from "./Containers/Album/Album";
import Tracks from "./Containers/Tracks/Tracks";
import Register from "./Containers/Login/Register";
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import SignUp from "./Containers/Login/SignUp";
import TrackHistory from "./Containers/TrackHistory/TrackHistory";

class App extends Component {
  render() {
    return (
      <Fragment>

          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Music</NavbarBrand>
            <NavbarToggler/>
            <Collapse navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink tag={RouterLink} to="/sign_up">Sign Up</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={RouterLink} to="/login">Register</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={RouterLink} to="/track_history">Track History</NavLink>
                </NavItem>

              </Nav>
            </Collapse>
          </Navbar>
        <Switch>
          <Route path='/album/:id' component={Albums}/>
          <Route path='/tracks/:id' component={Tracks}/>
          <Route path='/track_history' component={TrackHistory}/>
          <Route path='/sign_up' component={SignUp}/>
          <Route path='/login' component={Register}/>
          <Route path='/' exact component={Artists}/>
        </Switch>
      </Fragment>
    );
  }
}

export default App;