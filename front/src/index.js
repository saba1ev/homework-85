import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';

import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import artistReducer from "./store/reducers/artists";
import albumReducer from "./store/reducers/albums";
import trackReducer from "./store/reducers/tracks";
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware, ConnectedRouter} from 'connected-react-router';
import userReducer from "./store/reducers/user";
import historyReducer from './store/reducers/history';

const history = createBrowserHistory();

const rootReducer = combineReducers({
  artists: artistReducer,
  albums: albumReducer,
  tracks: trackReducer,
  user: userReducer,
  history: historyReducer,
  router: connectRouter(history)
});
const middleware = [
  thunkMiddleware,
  routerMiddleware(history)

];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancers = composeEnhancers(applyMiddleware(...middleware));



const store = createStore(rootReducer, enhancers);

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
