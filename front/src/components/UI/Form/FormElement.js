import React from 'react';
import {Col, FormFeedback, FormGroup, Input, Label} from "reactstrap";
import PropsTypes from 'prop-types';

const FormElement = props => {
  return (
    <FormGroup row>
      <Label for={props.propertyName} sm={2}>{props.title}</Label>

      <Col sm={10}>
        <Input
          type={props.type} required={props.required}
          id={props.propertyName} name={props.propertyName}
          value={props.value}
          placeholder={props.placeholder}
          invalid={!!props.error}
          onChange={props.onChange}
          autoComplete={props.autoComplete}
        />

        {props.error && (
          <FormFeedback>
            {props.error}
          </FormFeedback>
        )}
      </Col>
    </FormGroup>
  );
};

FormElement.propTypes = {
  propertyName: PropsTypes.string.isRequired,
  title: PropsTypes.string.isRequired,
  required: PropsTypes.bool,
  placeholder: PropsTypes.string,
  value: PropsTypes.string.isRequired,
  onChange: PropsTypes.func.isRequired,
  error: PropsTypes.string
};

export default FormElement;