import React, {Component} from 'react';
import {Alert, Button, Container, Form} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {loginUser} from "../../store/actions/UserAction";
import connect from "react-redux/es/connect/connect";

class SignUp extends Component {
  state = {
    username: '',
    password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.singUp({...this.state});

  };


  render() {
    return (
      <div>
        <Container>
          <h2>Sign Up</h2>
          { this.props.error &&
            <Alert color="danger">
              {this.props.error.error || this.props.error.global}
            </Alert>
          }
          <Form onSubmit={this.submitFormHandler}>
            <FormElement
              propertyName='username'
              type='text'
              title='Username'
              value={this.state.username}
              onChange={this.inputChangeHandler}
              autoComplete='current-username'
              placeholder='Enter your username'
            />

            <FormElement
              propertyName='password'
              type='password'
              title='Password'
              value={this.state.password}
              onChange={this.inputChangeHandler}
              autoComplete='current-password'
              placeholder='Enter your password'
            />
            <Button color='info' onClick={this.submitFormHandler}>Save</Button>
          </Form>

        </Container>
      </div>
    );
  }
}
const mapStateToProps = state =>({
  error: state.user.error
});
const mapDispatchToProps = dispatch =>({
  singUp: (userData)=> dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps) (SignUp);