import React, {Component} from 'react';
import {Alert, Button, Container, Form} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/UserAction";

class Register extends Component {
  state={
    username: '',
    password: ''
  };

  inputChangeHandler = event =>{
    this.setState({[event.target.name]: event.target.value})
  };
  submitFromHendler = event =>{
    event.preventDefault();
    this.props.register({...this.state});
  };
  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };
  render() {
    return (
      <Container>
        <h2>Register</h2>
        {(this.props.error && this.props.error.global) &&  (
          <Alert color="danger">
            {this.props.error.global}
          </Alert>
        )}
        <Form onSubmit={this.submitFromHendler}>
          <FormElement
            propertyName='username'
            type='text'
            title='Username'
            value={this.state.username}
            error={this.getFieldError('username')}
            onChange={this.inputChangeHandler}
            autoComplete='current-username'
            placeholder='Enter your username'
          />

          <FormElement
            propertyName='password'
            type='password'
            title='Password'
            value={this.state.password}
            error={this.getFieldError('password')}
            onChange={this.inputChangeHandler}
            autoComplete='current-password'
            placeholder='Enter your password'
          />
          <Button color='info' onClick={this.submitFromHendler}>Save</Button>
        </Form>

      </Container>
    );
  }
}
const mapStateToProps = state =>({
  error: state.user.registerError
});
const mapDispatchToProps = dispatch =>({
  register: (userData)=>dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps) (Register);