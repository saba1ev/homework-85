import React, {Component} from 'react';
import {connect} from "react-redux";
import {Card, CardTitle, Container} from "reactstrap";
import {showTracks} from "../../store/actions/TrackAction";
import {clickTracks} from "../../store/actions/TrackHistoryAction";


class Tracks extends Component {
  componentDidMount(){
    this.props.showTracksInfo(this.props.match.params.id);
  }
  render() {
    return (
      <div>
        <Container>
          <h2>Track List</h2>
          {this.props.tracks? this.props.tracks.map(item=> {
            return (
              <Card onClick={()=>this.props.clickTracks({tracks: item._id})} key={item._id} style={{"textAlign": "center"}}>
                <CardTitle>
                  <h3>{item.name}</h3>
                  <p>time: {item.duration}</p>
                  <strong>number: {item.number}</strong>

                </CardTitle>
              </Card>
            )
          }) : null}
        </Container>

      </div>
    );
  }
}
const mapStateToProps = state =>({
  tracks: state.tracks.tracks
});
const mapDispatchToProps = dispatch =>({
  showTracksInfo: (id) => dispatch(showTracks(id)),
  clickTracks: (id) => dispatch(clickTracks(id))
});

export default connect(mapStateToProps, mapDispatchToProps) (Tracks);