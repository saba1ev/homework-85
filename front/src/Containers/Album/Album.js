import React, {Component} from 'react';
import {connect} from "react-redux";
import {Card, CardImg, CardTitle, Container, NavLink} from "reactstrap";
import {NavLink as RouterLink} from 'react-router-dom'
import {showAlbums} from "../../store/actions/AlbumAction";

const local = "http://localhost:8000/uploads/";

class Albums extends Component {
  componentDidMount(){
    this.props.showAlbumsInfo(this.props.match.params.id);
  }
  render() {
    return (
      <div>
        <Container>
          <h2>Album List</h2>
          {this.props.albums? this.props.albums.map(item=> {
            return (
              <Card key={item._id} style={{"textAlign": "center"}}>
                <CardImg src={item.image ? local + item.image : null} style={{"width": "250px", "marginLeft": "40%"}} alt='Not pic'/>
                <CardTitle>
                  <NavLink tag={RouterLink} to={`/tracks/${item._id}`}>{item.name}</NavLink>
                  <p>Releas {item.releas}</p>

                </CardTitle>
              </Card>
            )
          }) : null}
        </Container>

      </div>
    );
  }
}
const mapStateToProps = state =>({
  albums: state.albums.albums,
});
const mapDispatchToProps = dispatch =>({
  showAlbumsInfo: (id) => dispatch(showAlbums(id))
});

export default connect(mapStateToProps, mapDispatchToProps) (Albums);