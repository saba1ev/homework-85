import React, {Component} from 'react';
import {showArtist} from "../../store/actions/ArtistAction";
import {connect} from "react-redux";
import {Card, CardImg, CardTitle, Container, NavLink} from "reactstrap";
import {NavLink as RouterLink} from 'react-router-dom'

const local = "http://localhost:8000/uploads/";

class Artists extends Component {
  componentDidMount(){
    this.props.showArtistInfo();
  }
  render() {
    return (
      <div>
        <Container>
          <h2>Artist's List</h2>
          {this.props.artists ? this.props.artists.map(item=> {
            return (
              <Card key={item._id} style={{"textAlign": "center"}}>
                <CardImg src={item.photo ? local + item.photo : null} style={{"width": "250px", "marginLeft": "40%"}} alt='Not pic'/>
                <CardTitle>
                  <NavLink tag={RouterLink} to={`/album/${item._id}`}>{item.name}</NavLink>
                </CardTitle>
              </Card>
            )
          }) : null}
        </Container>

      </div>
    );
  }
}
const mapStateToProps = state =>({
  artists: state.artists.artists
});
const mapDispatchToProps = dispatch =>({
  showArtistInfo: () => dispatch(showArtist())
});

export default connect(mapStateToProps, mapDispatchToProps) (Artists);