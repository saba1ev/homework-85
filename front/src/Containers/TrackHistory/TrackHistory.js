import React, {Component} from 'react';
import {Card, CardBody, CardTitle, Container} from "reactstrap";
import {connect} from "react-redux";
import {getTracks} from "../../store/actions/TrackHistoryAction";

class TrackHistory extends Component {
  componentDidMount(){
    console.log(this.props.getTracks());
  }
  render() {
    // console.log(this.props.getTracks())
    return (
      <Container>

          {this.props.history ? this.props.history.map(item=>(
            <Card>
              <CardTitle>
                {item.username}
              </CardTitle>
              <CardBody>

              </CardBody>
            </Card>
          )): null}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  history: state.history.history
});
const mapDispatchToProps = dispatch =>({
  getTracks: (id)=> dispatch(getTracks(id))

});

export default connect(mapStateToProps, mapDispatchToProps) (TrackHistory);