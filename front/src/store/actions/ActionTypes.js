export const FETCH_ARTISTS_REQUEST = "FETCH_ARTISTS_REQUEST";
export const FETCH_ARTISTS_SUCCESS = "FETCH_ARTISTS_SUCCESS";
export const FETCH_ARTISTS_FAILURE = "FETCH_ARTISTS_FAILURE";

export const FETCH_ALBUMS_REQUEST = "FETCH_ALBUMS_REQUEST";
export const FETCH_ALBUMS_SUCCESS = "FETCH_ALBUMS_SUCCESS";
export const FETCH_ALBUMS_FAILURE = "FETCH_ALBUMS_FAILURE";

export const FETCH_TRACK_REQUEST = "FETCH_TRACK_REQUEST";
export const FETCH_TRACK_SUCCESS = "FETCH_TRACK_SUCCESS";
export const FETCH_TRACK_FAILURE = "FETCH_TRACK_FAILURE";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const TRACK_HISTORY_SUCCESS = 'TRACK_HISTORY_SUCCESS';
export const TRACK_HISTORY = 'TRACK_HISTORY';