import {FETCH_ARTISTS_FAILURE, FETCH_ARTISTS_REQUEST, FETCH_ARTISTS_SUCCESS} from "./ActionTypes";
import axios from '../../axios-music';

export const fetch_artists_request = () =>(
  {type: FETCH_ARTISTS_REQUEST}
);
export const fetch_artists_success = (payload) =>(
  {type: FETCH_ARTISTS_SUCCESS, payload}
);
export const fetch_artists_failure = (failure) =>(
  {type: FETCH_ARTISTS_FAILURE, failure}
);

export const showArtist = () =>{
  return dispatch =>{
    dispatch(fetch_artists_request());
    axios.get('/artist').then(response=>{
      dispatch(fetch_artists_success(response.data))
    }, error=>{
      dispatch(fetch_artists_failure(error))
    })
  }
};
