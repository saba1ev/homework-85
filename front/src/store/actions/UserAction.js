import {LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, REGISTER_USER_FAILURE, REGISTER_USER_SUCCESS} from "./ActionTypes";
import {push} from 'connected-react-router';
import axios from '../../axios-music';

export const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

export const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};
export const loginUserSuccess = user => {
  return{type: LOGIN_USER_SUCCESS, user}
};
export const loginUserFailure = fail => {
  return{type: LOGIN_USER_FAILURE, fail}
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/user', userData).then(
      () => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
      },
      error => {
        dispatch(registerUserFailure(error.response.data))
      }
    );
  };
};
export const loginUser = userData => {
  return dispatch => {
    return axios.post('/user/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        dispatch(loginUserFailure(error.response.data))
      }
    );
  };
};