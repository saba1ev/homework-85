
import axios from '../../axios-music';
import {
  FETCH_TRACK_FAILURE,
  FETCH_TRACK_REQUEST,
  FETCH_TRACK_SUCCESS
} from "./ActionTypes";

export const fetchTrackRequest = () =>(
  {type: FETCH_TRACK_REQUEST}
);
export const fetchTrackSuccess = (payload) =>(
  {type: FETCH_TRACK_SUCCESS, payload}
);
export const fetchTrackFailure = (failure) =>(
  {type: FETCH_TRACK_FAILURE, failure}
);

export const showTracks = (id) =>{
  return dispatch =>{
    dispatch(fetchTrackRequest());
    axios.get('/track?album=' + id).then(response=>{
      dispatch(fetchTrackSuccess(response.data))
    }, error=>{
      dispatch(fetchTrackFailure(error))
    })
  }
};
