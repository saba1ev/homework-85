
import axios from '../../axios-music';
import {FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_REQUEST, FETCH_ALBUMS_SUCCESS} from "./ActionTypes";

export const fetchAlbumsRequest = () =>(
  {type: FETCH_ALBUMS_REQUEST}
);
export const fetchAlbumsSuccess = (payload) =>(
  {type: FETCH_ALBUMS_SUCCESS, payload}
);
export const fetchAlbumsFailure = (failure) =>(
  {type: FETCH_ALBUMS_FAILURE, failure}
);

export const showAlbums = (id) =>{
  return dispatch =>{
    dispatch(fetchAlbumsRequest());
    axios.get('/album?artist=' + id).then(response=>{
      dispatch(fetchAlbumsSuccess(response.data))
    }, error=>{
      dispatch(fetchAlbumsFailure(error))
    })
  }
};
