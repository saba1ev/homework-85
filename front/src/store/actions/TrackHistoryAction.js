import axios from 'axios';
import {TRACK_HISTORY, TRACK_HISTORY_SUCCESS} from "./ActionTypes";
import {push} from "connected-react-router"

export const trackHistorySuccess = playload =>{
  return {type: TRACK_HISTORY_SUCCESS, playload}
};

export const trackHistory = () => {
  return {type: TRACK_HISTORY}
};

export const getTracks = () =>{
  return (dispatch,getState) =>{
    const token = getState().user.user;
    axios.get('/history', {headers: {'Authorization': token}}).then(
      response => dispatch(trackHistorySuccess(response.data))
    )
  }
};
export const clickTracks = (id) => {
  return (dispatch, getState) =>{
    const users = getState().user.user;
    console.log(users);
    if (users === null){
      dispatch(push('/sign_up'))

    } else {
      return axios.post('/history', id, {headers: {'Authorization': users}}).then(
        () => dispatch(trackHistory())
      );
    }

  }
};