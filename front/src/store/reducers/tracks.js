import {
  FETCH_TRACK_FAILURE,
  FETCH_TRACK_REQUEST, FETCH_TRACK_SUCCESS
} from "../actions/ActionTypes";

const initialState = {
  tracks: null,
  error: null
};

const trackReducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_TRACK_REQUEST:
      return {...state};
    case FETCH_TRACK_SUCCESS:
      return {...state, tracks: action.payload};
    case FETCH_TRACK_FAILURE:
      return {...state, error: action.failure};
    default:
      return state
  }
};

export default trackReducer;