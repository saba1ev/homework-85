import {TRACK_HISTORY_SUCCESS} from "../actions/ActionTypes";


const initialState = {
  history: null
};

const TrackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case TRACK_HISTORY_SUCCESS:
      return {
        ...state,
        history: action.payload
      };
    default:
      return state;
  }
};

export default TrackHistoryReducer;