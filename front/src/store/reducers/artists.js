import {FETCH_ARTISTS_FAILURE, FETCH_ARTISTS_REQUEST, FETCH_ARTISTS_SUCCESS} from "../actions/ActionTypes";

const initialState = {
  artists: null,
  error: null
};

const artistReducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
      return {...state};
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artists: action.payload};
    case FETCH_ARTISTS_FAILURE:
      return {...state, error: action.failure};
    default:
      return state
  }
};

export default artistReducer;