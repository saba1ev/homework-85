import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actions/ActionTypes";

const initialState = {
  error: null,
  registerError: null,
  user: null
};

const userReducer = (state = initialState, action)=>{
  switch (action.type) {
    case REGISTER_USER_SUCCESS:
      return{
        ...state,
        registerError: null
      };
    case REGISTER_USER_FAILURE:
      return{
        ...state,
        registerError: action.error
      };
    case LOGIN_USER_SUCCESS:
      return{
        ...state,
        user: action.user,
        error: null
      };
    case LOGIN_USER_FAILURE:
      return{
        ...state,
        error: action.fail
      };
    default:
      return state
  }
};

export default userReducer;