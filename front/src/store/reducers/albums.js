import {FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_REQUEST, FETCH_ALBUMS_SUCCESS} from "../actions/ActionTypes";

const initialState = {
  albums: null,
  error: null
};

const albumReducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return {...state};
    case FETCH_ALBUMS_SUCCESS:
      return {...state, albums: action.payload};
    case FETCH_ALBUMS_FAILURE:
      return {...state, error: action.failure};
    default:
      return state
  }
};

export default albumReducer;